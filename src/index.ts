import * as Koa from 'koa';
// TODO: make session object interface
// TODO: use session middleware in koa app on high leve


const app = new Koa();
import userRouter from './router/user';
import * as bodyParser from 'koa-bodyparser';

const APP_PORT = process.env.PORT;
app.use(bodyParser());

// TODO: modify this middleware to make logging of requests
app.use((ctx, next) => {
  next()
});

app.use(userRouter.routes());

app.listen(APP_PORT, () => {
  console.log('server is listening on port ', APP_PORT);
});
