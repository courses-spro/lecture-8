import * as Router  from 'koa-router';
import { loginHandle } from '../controller/user';

const router = new Router({ prefix: '/user' });

// TODO: move handler to controller
router.get('/', (ctx) => {
  ctx.body = [{user: 'some user'}]
});


router.post('/login', loginHandle);

export default router;
